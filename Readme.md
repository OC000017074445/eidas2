# eIDAS2

Information zu Zielen, Rahmenbedingungen, ersten Überlegungen sowie zum Start des öffentlichen Konsultationsprozesses zu einem Konzept der deutschen Ausgestaltung von EUdi-Brieftaschen

# Zeitleiste des Konsultationsprozesses

Die **Kick-Off Veranstaltung** des Konsultationsprozesses findet am **31.07.** statt. 

Informationen zum weiteren Prozess, inklusive der anvisierten Termine für die themenspezifischen Workshops, welche ab dem Spätsommer beginnen sollen, entnehmen Sie bitte der beigefügten [**Zeitleiste**](https://gitlab.opencode.de/bmi/eidas2/-/raw/main/Zeitleiste_Konsultationsprozess.pdf)
![Zeitleiste](Zeitleiste_Konsultationsprozess.png)

# Antrag einreichen
Um einen Antrag einzureichen, bitten wir Sie unsere "Issue" Funktion zu nutzen.

Hierbei können Dokumente per Drag&Drop hinzugefügt werden.

![Einreichen](Antragseinreichung.gif)

# Beyond EU Digital Identity Wallet

> Diskussionspapier zur Erarbeitung einer prototypischen eIDAS 2.0-konformen Infrastruktur für Digitale Identitäten in Deutschland

## Ausgangslage und Zielsetzung

Mit der Einführung der eID-Funktion des Personalausweises hat die
Bundesregierung die Grundlage für eine digitale hoheitliche Identität
der Bürgerinnen und Bürger geschaffen, die hohen
Sicherheitsanforderungen gerecht wird.

Auch über die digitale hoheitliche „Kernidentität“ hinaus besteht der
Bedarf von Bürgerinnen und Bürgern sowie von staatlichen,
privatwirtschaftlichen und sonstigen Organisationen, dass weitere
digitale Nachweise rund um die Identität von natürlichen und
juristischen Personen erbracht werden können. Nachweise, die von
privatwirtschaftlichen oder staatlichen Akteuren herausgegeben werden,
die einzeln oder kombiniert, personengebunden oder pseudonym
weitergegeben werden können, um einzelne Aspekte einer Identität zu
bestätigen oder zu präsentieren. Digital muss mindestens das möglich
sein, was mit physischen Nachweisen auch möglich ist, einfach, sicher
und nutzerfreundlich. Darüber hinaus müssen die Möglichkeiten von
digitalen Verfahren genutzt werden, um Prozesse datensparsam,
fälschungssicher und datenschutzfreundlich zu gestalten, wo dies im
Vergleich mit der physischen Präsentation von Nachweisen möglich ist.
Eine (staatliche) Infrastruktur für digitale Identitäten ist die
Grundvoraussetzung zur Digitalisierung vieler Geschäfts- und
Verwaltungsprozesse und besitzt ein sehr hohes gesamtwirtschaftliches
Potential.

Auf europäischer Ebene gewinnt das Bestreben, vertrauenswürdige digitale
Identitätsnachweise zu realisieren und dadurch einen digitalen
Binnenmarkt zu befähigen, an Dynamik. Im Zuge der Novellierung der
eIDAS-Verordnung steht dabei im Mittelpunkt, dass das bestehende System
der grenzüberschreitenden Anerkennung von eID-Mitteln in zwei Richtungen
erweitert wird: Zum einen sollen weitere Nachweise ergänzt werden, zum
anderen soll die grenzüberschreitende Identifizierung und
Authentifizierung nicht nur gegenüber staatlichen Stellen (wie in der
eIDAS-Verordnung von 2014), sondern auch gegenüber
privatwirtschaftlichen Organisationen sichergestellt werden. Dabei soll
ein Rahmen für sogenannte „EU Digital Identity Wallets“
(EUdi-Brieftaschen) geschaffen werden. Die Ausgestaltung ist im Rahmen
der noch zu finalisierenden EU-Vorgaben den Mitgliedsstaaten überlassen,
die eine solche Lösung entweder selbst bereitstellen, jemanden dafür
beauftragen, oder bestehende/unabhängig entwickelte Lösungen
zertifizieren müssen.

Mit der Konzeption und Umsetzung einer prototypischen (staatlichen)
Infrastruktur für digitale Identitäten wird das Ziel verfolgt, die
Bedarfe aus der novellierten eIDAS-Verordnung (eIDAS-VO 2.0)
aufzugreifen und Anforderungen an das bestehende deutsche eID-System zu
formulieren. Dabei soll an den Stärken des bestehenden eID-Systems und
der dezentralen deutschen Registerlandschaft angeknüpft und diese
weiterentwickelt werden. Nach Abschluss der Konzeptionsphase soll die
prototypische Entwicklung und Testung folgen. Diese geplante
prototypische Infrastruktur für eine EUdi-Brieftasche soll zur Erprobung
von konkreten Anwendungsfällen verwendet werden. Konkret sollen die
Partner des POTENTIAL-Konsortiums für die „Large Scale Pilots der
European Digital Identity Wallet“ (LSP) die Möglichkeit erhalten, eine
solche Integration zu testen.

Mit den LSP sollen europaweit die Möglichkeiten einer EUdi-Brieftasche
dargestellt und die Funktionalitäten sowie deren Mehrwert anhand
verschiedener alltagsrelevanter grenzübergreifender Anwendungsfälle
demonstriert werden (vgl. Kapitel [
Abgrenzung zu den Large Scale Pilots (LSP)](#abgrenzung-zu-den-large-scale-pilots-lsp)). Erkenntnisse aus der Entwicklung
werden in die weitere Ausgestaltung des Architecture and Reference
Framework (ARF) auf EU-Ebene eingebracht. Das ARF bildet das Fundament
der technologischen Infrastruktur und deren Implementierung und wird von
einer Expertengruppe der EU gemeinsam mit den Mitgliedsstaaten
erarbeitet. Aus dem ARF werden die Durchführungsrechtsakte und die
technischen Vorgaben für die eIDAS-Referenzwallet abgeleitet. Die
Absicht ist eine aktive Mitgestaltung des ARF mit Hilfe der Erfahrungen
bei der Implementierung des eigenen Prototyps. Die Bunderegierung bringt
sich im französisch-deutsch geführten Konsortium „POTENTIAL“ bei den
Large Scale Pilots ein (vgl. Kapitel [
Abgrenzung zu den Large Scale Pilots (LSP)](#abgrenzung-zu-den-large-scale-pilots-lsp)). Für Bürgerinnen
und Bürger sowie Organisationen bringt diese Entwicklung eine
Erleichterung für den digitalen Zugang zu Diensten von Verwaltung und
Wirtschaft mit sich. Ebenso kann die Privatwirtschaft profitieren, indem
Geschäftsprozesse zunehmend digitalisiert, Kosten eingespart und
möglichen betrügerischen Vorgängen im digitalen Raum vorgebeugt bzw.
entgegengewirkt werden können. Für den deutschen Staat wird dies zu
einer weiteren Beschleunigung der Verwaltungsdigitalisierung führen.

Zur Umsetzung dieses Vorhabens plant das Bundesministerium des Innern
und für Heimat (BMI) im Rahmen des GovLabDE Digitale Identitäten einen
offenen Architektur- und Entwicklungsprozess. Der Prozess soll
transparent und partizipativ gestaltet werden. Dieses Papier ist als
Diskussionsgrundlage und -anstoß gedacht und stellt den Start eines
Konsultationsprozesses (vgl. Kapitel [Offener Architekturprozess](#offener-architekturprozess)) dar. Das Papier kann als Aufruf
des BMI zum Austausch verstanden werden, zum Austausch mit
Vertreterinnen und Vertretern von Verbänden, Zivilgesellschaftlichen
Organisationen, Unternehmen, Expertinnen und Experten aus der
Wissenschaft, aber auch mit Partnerinnen und Partnern aus Staat und
Verwaltung, aus Ländern und Kommunen.

Das Hauptziel dieses Papiers besteht darin, frühestmöglich die
Öffentlichkeit sowie interessierte Expertinnen und Experten in den
Prozess der Konzeptionierung und Entwicklung einer eIDAS
2.0-kompatiblen, prototypischen Infrastruktur einzubinden und einen
nutzerzentrierten, anwenderfreundlichen sowie sicheren,
datenschutzfreundlichen und wirtschaftlichen Ansatz zu gewährleisten.
Durch die frühzeitige Einbindung sollen wichtige Erkenntnisse für
zukünftige Architekturen und für künftige Konsultations- und
Beteiligungsprozesse gesammelt werden.

## Rahmenbedingungen und Abgrenzung

### Online-Ausweisfunktion

Seit 2010 gibt es den Online-Ausweis im Chip des deutschen
Personalausweises. 2011 kam der elektronische Aufenthaltstitel hinzu,
2021 die eID-Karte für Bürgerinnen und Bürger der Europäischen Union.
Insgesamt wurden bis Februar 2023 94,8 Mio. Personalausweise ausgegeben.
Bei 52,3 Mio. Personalausweisen ist der Online-Ausweis aktiviert. Der
aktuelle Gesetzesentwurf zur Modernisierung des Pass- und Ausweiswesens
sieht eine Senkung des Mindestalters für die Nutzung des
Online-Ausweises von 16 auf 13 Jahre vor. Hierdurch soll eine sichere
Nutzung, der für Jugendliche zugänglichen Plattformen und sozialen
Medien gewährleistet werden. Zudem kann die die Gesetzesänderung
kurzfristig zu weiteren Anwendungsfällen, sowie zu einer Erhöhung der
potenziellen Nutzeranzahl führen.

Der Online-Ausweis kann über ein NFC-fähiges Smartphone, Tablet oder
Kartenlesegerät ausgelesen werden. Dafür halten Nutzende das
Ausweisdokument an die NFC (Near Field Communication)-Schnittstelle des
Smartphones bzw. Tablets oder verbinden es mit dem Kartenlesegerät, um
eine sichere Verbindung herzustellen. Für das Auslesen benötigen sie
eine Software für das Online-Ausweisen (beispielsweise die AusweisApp2)
und die PIN ihres Online-Ausweis.

Bald werden sich Bürgerinnen und Bürger mit der Komfortfunktion, der
sog. Smart-eID, auf ihren Smartphones ohne ihre Ausweiskarte online
ausweisen können. Das Gesetz zur Einführung eines elektronischen
Identitätsnachweises mit einem mobilen Endgerät (Smart-eID-Gesetz) trat
am 01.09.21 in Kraft.

Weitere Informationen zum deutschen eID-System sowie wichtige
Hintergrundinformationen zur eIDAS-Notifizierung hat das Bundesamt für
Sicherheit in der Informationstechnik übersichtlich und umfangreich
[hier](https://www.bsi.bund.de/DE/Themen/Oeffentliche-Verwaltung/Elektronische-Identitaeten/Online-Ausweisfunktion/eIDAS-Notifizierung/eidas-notifizierung_node.html) beschrieben.

### eIDAS-Verordnung 2.0

Die Verordnung über elektronische Identifizierung und Vertrauensdienste
für elektronische Transaktionen im Binnenmarkt (eIDAS) aus dem Jahr
2014, schafft einheitliche Rahmenbedingungen für die zwischenstaatliche
Nutzung elektronischer Identifizierungsmittel und Vertrauensdienste.

Die eIDAS-Verordnung 2.0 reformiert diese Regulierung und legt die
Grundlagen zur verstärkten Nutzung elektronischer
Identifizierungsmittel. Den Kern der Verordnung bildet die Festlegung
der Bedingungen für die Erteilung, Verwaltung und Anerkennung einer
EUdi-Brieftasche durch die EU-Mitgliedstaaten, über die aktuell zwischen
EU-Parlament, EU-Kommission und Rat der Europäischen Union als ein
eigenständiges elektronisches Identifizierungsmittel neben den
bisherigen eID-Systemen verhandelt wird. Darüber hinaus soll die
EUdi-Brieftasche den Nutzenden die Möglichkeit bieten zusätzliche
(qualifizierte) Attribute (z.B. den Führerschein, behördliche
Genehmigungen oder Geburtsurkunden oder Hochschulzeugnisse) zu teilen,
sowie Dokumente qualifiziert elektronisch zu signieren.

Die eIDAS-Verordnung 2.0 befindet sich momentan (Stand: Juni 2023) in
der Trilog-Phase, in der das EU-Parlament, die EU-Kommission und der
EU-Ministerrat über die Änderungen in der Verordnung verhandeln. Ein
Großteil der Rahmenbedingungen für eine Infrastruktur für die
Bereitstellung einer EUdi-Brieftasche sind dabei unstrittig und werden
im Verordnungsvorschlag der **EU-Kommission** sowie der
Allgemeinen Ausrichtung des **Rates der Europäischen Union** und
der Position des **EU-Parlaments** hierzu, die öffentlich
zugänglich sind, gleich oder ähnlich beschrieben. Eine Reihe von
Eckpunkten werden noch im Rahmen des Trilogs geklärt, zum Beispiel:

- die Verpflichtung der Mitgliedstaaten zur Einführung einer
  eindeutigen, dauerhaften Personenkennung (Unique Persistent
  Identifier, UPI)
- der Zugriff auf Secure Elements (eine manipulationssichere Hardware,
  meist ein sicherer Ein-Chip-Mikrocontroller, die in der Lage ist,
  Applets und deren vertrauliche und kryptografische Daten sicher zu
  hosten), um Hardware-Sicherheit EU-weit zu gewährleisten und auf dem
  Endgerät Daten souverän und vertrauensvoll ablegen zu können
- Peer-to-Peer Verbindung zwischen EUdi-Brieftaschen
- verpflichtende Open-Source-Bereitstellung der Codes der jeweiligen
  technischen Umsetzung
- die Rolle von Pseudonymen bei elektronischen Transaktionen
- Kostenfreiheit für Bürgerinnen und Bürger bei qualifizierten
  elektronischen Signaturen (QES)
- Zertifizierung von Brieftaschen durch Mitgliedstaaten
- Einrichtung und Benennung nationaler Behörde(n) zur Überwachung und
  Durchführung der Anwendung von Infrastruktur-Komponenten nach eIDAS
  2.0 in den Mitgliedstaaten
- Umsetzungsfrist für die Implementierung

Den Mitgliedstaaten stehen dabei drei Optionen zur Verfügung, in welcher
Form EUdi-Brieftaschen herausgegeben werden können:

- durch den Mitgliedstaat;
- im Auftrag des Mitgliedstaates; oder
- unabhängig vom Mitgliedstaat, aber von einem Mitgliedstaat anerkannt.

Die Verabschiedung der neuen Verordnung ist noch im Jahr 2023 geplant,
darauf folgen Durchführungsverordnungen der EU-Kommission und je nach
Umsetzungsfrist dann die Verpflichtung der Mitgliedsstaaten zur
Implementierung.

### Überarbeitung der Führerscheinrichtlinie 

Die EU-Kommission stellte am 01.03.2023 einen ersten Entwurf für die 4.
EU-Führerscheinrichtlinie vor, die auch die Einführung eines digitalen
Führerscheins auf EU-Ebene als Ergänzung zur physischen Variante
vorsieht. Die Initiative wird den neuen Herausforderungen für die
Mobilität, insbesondere im digitalen Bereich, Rechnung tragen und zu den
Zielen der EU beitragen, die in der Strategie für intelligente und
nachhaltige Mobilität von 2020 festgelegt sind.

Die 4. EU-Führerscheinrichtlinie ist neben der eIDAS-Verordnung (vgl. Kapitel [eIDAS-Verordnung 2.0](#eidas-verordnung-20)) die wichtigste rechtliche Grundlage für die Umsetzung und
Gestaltung eines zukünftigen mobilen Führerscheins (auch mobile Driving
License, mDL). Die mDL soll als ein eigenständiges, elektronisches
hoheitliches Dokument gelten, mit digitaler Repräsentation sowie
Überprüfung auf Echtheit und Gültigkeit unabhängig des
Identifizierungsmittels. Dieser soll online und offline für den Nachweis
der Fahrerlaubnis genutzt werden können. Zunächst müssen jedoch die sich
aus der 4. Führerscheinrichtlinie sowie aus den Bedürfnissen der
Nutzenden ergebenden Anforderungen mit Hinblick z.B. auf den
Kopierschutz konkretisiert werden.

### Abgrenzung zu den Large Scale Pilots (LSP)

Mit den Large Scale Pilots (LSP) für die European Digital Identity
Wallet sollen europaweit die Möglichkeiten einer EUdi-Brieftasche
dargestellt und die Funktionalitäten sowie deren Mehrwert anhand
verschiedener alltagsrelevanter grenzübergreifender Anwendungsfälle
demonstriert werden. Hierbei werden regulatorische und technische
Rahmenbedingungen für die erfolgreiche Implementation von möglichen
Anwendungsfällen für eIDAS 2.0 identifiziert.

Hierfür hat neben drei weiteren Konsortien das Konsortium „POTENTIAL“
den Zuschlag bei einer Ausschreibung der EU-Kommission erhalten. Das
französisch-deutsch geführte Konsortium besteht aus 148 öffentlichen und
privatwirtschaftlichen Partnern aus 20 Staaten (19 EU-Mitgliedstaaten +
Ukraine).

Die überarbeitete eIDAS-Verordnung sieht perspektivisch vor, dass die
EUdi-Brieftasche von den Mitgliedstaaten bereitgestellt bzw. anerkannt
wird. Die Rahmenbedingungen werden, wie im Kapitel [eIDAS-Verordnung 2.0](#eidas-verordnung-20) erläutert, in der
Verordnung beschrieben. Das Fundament der technologischen Infrastruktur
und deren Implementierung bildet das ARF, das von einer Expertengruppe
der EU gemeinsam mit den Mitgliedsstaaten erarbeitet wird. Aus den
Arbeiten der Expert Group/dem ARF werden die Durchführungsrechtsakte und
die technischen Vorgaben für allgemeine Brieftaschen und die
eIDAS-Referenzwallet abgeleitet. Letztere wird im Auftrag der
EU-Kommission durch „NiScy“ (Netcompany-Intrasoft & Scytáles AB)
entwickelt und den LSP-Konsortien als Open Source zur Verfügung
gestellt. Komponenten der Referenzwallet können auch in den deutschen
Prozess einfließen. Dabei ist jedoch zu beachten, dass diese
Referenzlösung auf EU-Ebene generisch sein wird und weder die
Gegebenheiten der jeweils bestehenden eID-Infrastruktur noch der
spezifischen (föderalen) Registerlandschaften der Mitgliedsstaaten
berücksichtigen wird. Ebenso ist zu berücksichtigen, dass die
Referenzwallet lediglich als Code zur Verfügung stehen und nicht als
fertiges Produkt bzw. App für den Betrieb bereitgestellt werden wird.

Die LSP-Projekte sollen in erster Linie die Möglichkeiten und die
Funktionalitäten einer EUdi-Brieftasche in verschiedenen öffentlichen
und privatwirtschaftlichen Anwendungsfällen veranschaulichen und die
Integration der zu schaffenden Infrastruktur in die Systeme von
Diensteanbietern demonstrieren. Im Rahmen der LSP-Projekte werden im
Zeitraum von Juni 2023 bis Juni 2025 Anwendungsfälle in der Praxis
getestet, dafür wird der entstehende Prototyp der EUdi-Brieftasche
genutzt werden können. Auf diese Weise unterstützt das
POTENTIAL-Konsortium den Aufbau von Expertise für die zukünftige
Einführung von EUdi-Brieftaschen. Der Mehrwert der Pilotierung besteht
darüber hinaus darin, dass die Möglichkeiten einer eIDAS 2.0-konformen
Infrastruktur für öffentliche Institutionen und privatwirtschaftliche
Akteure sowie Bürgerinnen und Bürger aufgezeigt und getestet werden. Die
LSP-Projekte bieten somit vor allem die Möglichkeit der Testung der zu
entwickelnden Lösungen mit Fokus auf Interoperabilität und
Skalierbarkeit in einem grenzüberschreitenden Kontext.

Insbesondere die Ausgestaltung der Schnittstellen für die
grenzüberschreitende Nutzung, die im Rahmen der LSP erfolgt, wird von
der nationalen Entwicklung einer prototypischen Infrastruktur
aufzugreifen sein. Dahingegen grenzen sich die Arbeiten der Large Scale
Pilots von den Entwicklungen einer prototypischen Infrastruktur für
EUdi-Brieftaschen wie folgt ab:

Die Arbeiten im Konsortium zu den Large Scale Pilots:

- Konzeption einer grenzüberschreitenden, prototypischen Infrastruktur für die Anwendungsfälle, samt Definition von entsprechenden Anforderungen an eine Umsetzung
- Test von EUdi-Brieftaschen und deren Hintergrundsystemen
- Implementierung von Anwendungsfällen und User Journeys
- Verprobung der Lösungen im grenzüberschreitenden Kontext
- Generierung von Feedback für das ARF und die Referenz-Wallet sowie für die deutsche Entwicklung.
- Erarbeitung der Anforderungen an die EUdi-Brieftasche, deren Architektur und Infrastruktur.

Die Arbeiten zur Bereitstellung einer prototypischen eIDAS 2.0-konformen
Infrastruktur und eines Prototypens für eine EUdi-Brieftasche:

- Konzeption, Entwicklung und Implementierung eines prototypischen
  Gesamtsystems, inklusive einer Brieftasche, welches das bestehende
  eID-System in Deutschland hin zu eIDAS 2.0-Konformität anpasst
- Generieren von Erkenntnissen zur Weiterentwicklung des ARF
- Bereitstellung einer technischen Infrastruktur für die deutschen
  Partner im LSP-Konsortium POTENTIAL sowie Bereitstellung einer
  grenzüberschreitenden Infrastruktur für die LSP-Anwendungsfälle.

## Anforderungen an die deutsche eIDAS 2.0-Infrastruktur

Ausgangspunkt für eine erste Diskussion zur Konzeption einer deutschen
Infrastruktur für Digitale Identitäten, die eIDAS 2.0-konform ist, sind
die Punkte, die sich aus der eIDAS-Verordnung 2.0 ableiten lassen. Hier
soll angesetzt werden und eine Bedarfsanalyse erfolgen, um die realen
Bedarfe von Bürgerinnen und Bürgern sowie von Unternehmen und
Organisationen zu ermitteln und die Anforderungen von europäischer Ebene
mit dem Bedarf in Deutschland zusammen zu bringen.

Die Anforderungen sollen im Austausch mit der Öffentlichkeit entwickelt
und im Rahmen der regulatorischen Vorgaben weiter spezifiziert werden,
um daraus konkrete Umsetzungs- und Entwicklungsschritte abzuleiten.

- **Regulatorische Anforderungen**: Mit der aktuell laufenden
  eIDAS-Novellierung werden der Rechtsrahmen und die regulatorischen
  Anforderungen an die Infrastruktur der Mitgliedstaaten vorgegeben. Das
  parallel zur eIDAS-Novellierung erarbeitete ARF-Dokument umreißt
  zusätzlich konkrete, technische Anforderungen, die jedoch zunächst
  keinen verpflichtenden Charakter haben. Ergänzend wird die Europäische
  Kommission eine Referenzimplementierung einer eIDAS-konformen
  EUdi-Brieftasche bereitstellen. Hinsichtlich des Anwendungsfalls
  Digitaler Führerschein sind die Vorgaben der sich derzeit noch im
  Rechtssetzungsverfahren befindlichen 4. EU-Führerscheinrichtlinie
  sowie der eIDAS-Verordnung 2.0 maßgeblich. Weitere Anforderungen
  resultieren aus den kommenden Durchführungsrechtsakten sowie aus dem
  europäischen Datenschutzrecht (DSGVO). Auf nationaler Ebene sind neben
  dem Vertrauensdienstegesetz und dem Personalausweisgesetz
  Anforderungen an die IT-Sicherheit (bspw. IT-Grundschutz & technische
  Richtlinien des BSI) zu berücksichtigen. Die technische und die
  regulatorische Ausgestaltung in Deutschland sollen dabei parallel und
  aufeinander abgestimmt erfolgen. Auch die Schnittstelle zur
  Registermodernisierung/ NOOTS Architektur muss bei der Konzeption mit
  betrachtet werden.

- **Anforderungen aus Sicht der Nutzenden**: Um eine breite Akzeptanz
  der Lösung bei den Nutzenden zu erreichen, ist es erforderlich, dass
  sich die Lösung an deren Bedarf hinsichtlich der Features und des
  Nutzungserlebnisses orientiert. Daraus resultieren Anforderungen, die
  eine intuitive Bedienbarkeit und ein positives Nutzungserlebnis
  gewährleisten. Auch die Barrierefreiheit wird hierbei berücksichtigt.
  Des Weiteren ist für die angestrebte breite Akzeptanz auch die
  Einbindung von Anwendungsfällen privatwirtschaftlicher
  Diensteanbieter, wie sie unter anderem in dem vom BMWK geförderten
  Schaufensterprogramm „Sicherer Digitale Identitäten“ erprobt werden,
  förderlich. Die Unbeobachtbarkeit von Nutzerinteraktionen soll eine
  Nachverfolgbarkeit von Transaktionen für den Staat oder sonstige
  Dritte ausschließen. Darüber hinaus muss die Einhaltung der
  Wahlfreiheit zur Nutzung einer EUdi-Brieftasche sichergestellt werden.

- **Anforderungen der Bundesregierung**: Mit dem GovLabDE Digitale
  Identitäten hat die Bundesregierung den interministeriellen Austausch
  zu digitalen Identitäten institutionalisiert. In diesem Rahmen haben
  sich die Ressorts auf eine gemeinsame Zieldefinition verständigt,
  welche die Anforderungen an die Realisierung von Lösungen im Bereich
  der digitalen Identitäten beschreibt. So sollen Lösungen unter dem
  Leitsatz „Privacy und Security by Design“ entwickelt, eine breite
  Nutzbarkeit der Öffentlichkeit sichergestellt und Implementierungen
  als Open Source bereitgestellt werden, u.a. um
  Nachnutzungsmöglichkeiten einzuräumen. Ansätze, die auf Distributed
  Ledger Technology (DLT) beruhen, werden in der Zieldefinition für
  Umsetzungen Lösungen im Bereich der Digitalen Identitäten auf
  absehbare Zeit ausgeschlossen. Ergänzend zu dieser Zieldefinition sind
  konkrete Anforderungen an die Lösung hinsichtlich z. B. Verfügbarkeit,
  Skalierbarkeit, etc. abzustimmen.

- **Anforderungen der internationalen Standardisierung**: Bei der
  Realisierung müssen die relevanten internationalen Standards
  berücksichtigt und ggf. neue Standards entwickelt werden, insbesondere
  um die grenzüberschreitende Nutzung und Interoperabilität mit gängigen
  Endgeräten technisch zu befähigen.

- **Anforderungen bestehender Infrastrukturen**: Die zu schaffende
  Infrastruktur wird nicht auf der grünen Wiese entstehen, sondern soll
  sich in bestehende Infrastrukturen (z. B. Online-Ausweisfunktion,
  föderale Registerlandschaft, eIDAS 1.0-Infrastruktur) einfügen und mit
  Infrastrukturen im EU-Ausland interagieren, woraus Anforderungen an
  die zu entwickelnde Lösung entstehen.

- **Anforderungen der Pilot-Anwendungsfälle im EU Large Scale Pilot**:
  Durch die Konzeption der Anwendungsfälle des POTENTIAL-Konsortiums mit
  deutscher Beteiligung im EU-Large Scale Pilot entstehen konkrete
  Anforderungen an eine Infrastruktur, die sich im Zuge der laufenden
  Spezifikation der Anwendungsfälle entwickeln.

## Ökosystem für Digitale Identitäten

Unter Auflage der aktualisierten eIDAS-Verordnung werden die
EU-Mitgliedsstaaten verpflichtet, ihren Bürgerinnen und Bürgern sowie
Organisationen eine EUdi-Brieftasche kostenlos zur Verfügung zu stellen.
Die Nutzung bleibt freiwillig. Für die Ausgestaltung einer solchen
Brieftasche ist den Mitgliedsstaaten ein hohes Maß an
Gestaltungsfreiraum und somit eine Bandbreite an Gestaltungsoptionen
geboten. Zusätzlich bieten sich den Mitgliedsstaaten mehrere Optionen,
in welcher Form sie die EUdi-Brieftasche ausgeben (vgl. Kapitel [eIDAS-Verordnung 2.0](#eidas-verordnung-20)). Erste
Überlegungen zur Ausgestaltung eines solchen Ökosystems führen zu drei
exemplarischen Wegen für eine Lösung in Deutschland, die im weiteren
Verlauf u.a. diskutiert werden sollen:

**1) „Staatliche Wallet als Ökosystem“**: Die erste Option versteht die
„Wallet als Ökosystem“ und sieht die Bundesregierung in der
Verantwortung zum Aufbau eines Ökosystems an Diensten und verschiedenen
Attributen auf Basis einer, von der Bundesregierung bereitgestellten,
Brieftasche, welche all diese Geschäftsprozesse ermöglichen soll. Somit
obliegen die Konzeption, Ausgestaltung und Bereitstellung der deutschen
EUdi-Brieftasche ausschließlich der Bundesregierung. Die
EUdi-Brieftasche ist ein Ökosystem, weil unterschiedliche Parteien, auch
privatwirtschaftliche Akteure, im Auftrag des Nutzers Attribute in die
Brieftasche zur späteren Verwendung einbringen können.

**2) „Ein Ökosystem von privatwirtschaftlichen Wallets“**: Alternativ
geschieht die Entwicklung der EUdi-Brieftasche innerhalb eines
Ökosystems, in dem verschiedene privatwirtschaftliche Organisationen
eigene EUdi-Brieftaschen bereitstellen können. Diese Brieftaschen werden
dann unter Regulatorischen Anforderungen (Lizenzierung, Zertifizierung,
etc.) von der Bundesregierung anerkannt werden können.

**3) Kombinationen der genannten Varianten**: Eine Kombination aus
staatlich und privatwirtschaftlich angebotenen EUdi-Brieftaschen bzw.
Modulen/SDKs und Komponenten einer (deutschen) EUdi-Brieftasche sind
ebenso möglich. Der Kern der Infrastruktur sowie eine nutzbare
Referenzimplementierung kann staatlich bereitgestellt werden. Daneben
ist die Entwicklung von kompatiblen Brieftaschen allen Marktteilnehmern
möglich. Letztere können anschließend zertifiziert werden.

Der erfolgreiche Aufbau des Ökosystems erfordert in allen Varianten eine
enge Zusammenarbeit von Staat, Wirtschaft und Zivilgesellschaft. Die
Förderung von Wettbewerb und Innovation durch die Einbindung der
Privatwirtschaft sind Argumente, die hier genauso überprüft werden
müssen, wie die Form des Betriebes von (digitaler) Basisinfrastruktur.
Die Ausgabe hoheitlicher Dokumente wird weiterhin ausschließlich durch
den Staat durchgeführt. Darüber hinaus soll betrachtet werden, durch
welche Zielmodelle die Adoption durch die Nutzenden und die Erreichung
des gesamtwirtschaftlichen Potentials bestmöglich gefördert werden kann.
Die Interessen der Bürgerinnen und Bürger, die Form und Ausgestaltung
von möglichen Geschäftsmodellen, Kostenstrukturen, aber auch die
Verantwortung, Haftung, Pflichten und hoheitlichen Aufgaben der
beteiligten Stellen sollen im Rahmen des nun initiierten Prozesses offen
diskutiert werden. Eine endgültige Entscheidung wird anschließend, unter
anderem auf Grundlage der gewonnenen Erkenntnisse, durch die
Bundesregierung getroffen.


## Funktionen und Use Cases einer EUdi-Brieftasche

### Schaubild der Module

Ohne Entscheidungen vorweg nehmen zu wollen, können die folgenden
enthaltenen Module einer EUdi-Brieftasche zusammengefasst werden. Dabei
sind Issuer als Dienste zu verstehen, die entsprechende Nachweise
ausgeben (z.B. staatliche Register), eine Relying Party ist ein
Diensteanbieter (unabhängig davon in welchem EU-Mitgliedstaat dieser
angesiedelt/registriert ist), das QES-Modul erlaubt das Auslösen
qualifizierter elektronischer Signaturen und Siegel – wahlweise mittels
eines Fernsignatur– oder Siegeldienstes eines qualifizierten
Vertrauensdiensteanbieters (QVDA). Die Personenidentifizierungsdaten
stammen aus der eID und werden in der Brieftasche verwendet. Auf hohem
Vertrauensniveau können diese auch nicht abgeleitet werden, sondern
verbleiben wahlweise auf der Chipkarte, im Secure Element oder der eSIM
(fest verbaute SIM-Karte):

![Schaubild der Module](./modules_overview.png)

Im weiteren Prozess sollen grobe Architekturskizzen erstellt und
diskutiert werden, dabei ist auch die Nutzung der EUdi-Brieftasche in
geräteübergreifenden Anwendungsfällen zu berücksichtigen. Dazu wird es
eigene Vorschläge von Seiten des BMI geben, jedoch ist jede\*r herzlich
eingeladen, selbst Architekturskizzen beizutragen und zur Diskussion zu
stellen. Mehr dazu unter [Studie zur Umsetzung von eIDAS 2.0](#studie-zur-umsetzung-von-eidas-20).

### Erläuterung der prioritären Use Cases 

Die deutsche Ausgestaltung einer Infrastruktur auf die ein Produkt oder
mehrere Produkte mit dem Titel „EUdi-Brieftasche“ aufsetzen, umfasst die
im eIDAS-Framework vorgesehenen Funktionen Identifizierung und
Authentifizierung gegenüber Diensteanbietern, das Speichern, Teilen und
Verwalten von Nachweisen sowie das Signieren von Dokumenten mittels QES.
Perspektivisch sind auch eine Bezahlfunktion und weitere
Funktionalitäten geplant. Folgende Anwendungsfälle soll der Prototyp
einer deutschen EUdi-Brieftasche grenzüberschreitend abdecken können, da
dies die Anwendungsfälle sind, die im LSP POTENTIAL-Konsortium unter
Beteiligung der Bundesregierung erprobt werden:

1.  ***eGovernment***: Bürgerinnen und Bürger sollen zukünftig von
    erleichterter und effizienterer Nutzung von Verwaltungsleistungen
    profitieren. Das vereinfachte Identifizierungs-, Autorisierungs- und
    Authentifizierungsverfahren soll es den Nutzenden ermöglichen,
    digitale Nachweise, die sie von öffentlichen Stellen erhalten
    (Bescheide, Urkunden, etc.) abzurufen und für die Verwendung in
    Online- und Vor-Ort-Szenarien mit der EUdi-Brieftasche vorzulegen.

2.  ***Online-Kontoeröffnung***: Den Nutzenden soll eine schnelle und
    intuitive Lösung für die Online-Kontoeröffnung zur Verfügung
    gestellt werden. Mithilfe der EUdi-Brieftasche können sie eine
    Online-Eröffnung eines Giro-, Spar- oder Depotkontos
    Geldwäschegesetz-konform durch die Verwendung ihrer
    Personenidentifizierungsdaten (PID) mittels Online-Ausweisfunktion
    veranlassen. Konkret würde die Lösung den Finanzinstituten eine
    Möglichkeit bieten ihren regulatorischen Verpflichtungen
    nachzukommen und Betrugsversuche weiter zu minimieren. Für die
    Bürgerinnen und Bürger bedeutet dies eine schnelle und
    benutzerfreundliche Prozessabwicklung, die ohne persönlichen Kontakt
    umgesetzt werden kann.

3.  ***Elektronische Identifizierung für die SIM-Karten
    Registrierung***: Mit der EUdi-Brieftasche können Bürgerinnen und
    Bürger zukünftig SIM-Karten registrieren und aktivieren und dazu
    Personenidentifizierungsdaten (PID) mittels Online-Ausweisfunktion
    online übertragen. Ziel ist es, ein durchgängiges digitales
    Registrierungsverfahren zu etablieren, das es den
    Mobilfunkbetreibern ermöglicht, ihren regulatorischen
    Verpflichtungen einfacher und kostengünstiger nachzukommen und
    gleichzeitig das Betrugsrisiko zu verringern. Für Bürgerinnen und
    Bürger bedeutet dies einen schnelleren und benutzerfreundlichen
    Prozess mit einer rund um die Uhr verfügbaren Lösung.

4.  ***Digitaler Führerschein (mDL)***: Durch die 4.
    EU-Führerscheinrichtlinie (derzeit noch im Entwurf) wird die
    Einführung eines Digitalen Führerscheins verpflichtend. Dabei muss
    auch die Durchführung von Offline-Verkehrskontrollen möglich sein.
    Zudem können die Nutzenden z.B. bei Autovermietungen von einem
    verbesserten und effizienten Prozess zur Überprüfung ihrer aktuellen
    Fahrerlaubnis profitieren. Gleichzeitig trägt die Implementierung
    des mDL zur Sicherheit, Zuverlässigkeit und Effizienz des
    Mietprozesses bei.

5.  ***Qualifizierte Elektronische Signatur und Siegel (QES)***: Die
    qualifizierte elektronische Signatur ist dem Gesetz nach mit der
    handschriftlichen Unterschrift gleichgestellt und die sicherste Form
    der elektronischen Signatur. Die EUdi-Brieftasche soll den Nutzenden
    ermöglichen, die qualifizierten elektronischen Signaturen
    auszulösen. Dies bedeutet, dass die Bürgerinnen und Bürger die
    Möglichkeit erhalten, qualifizierte elektronische Signaturen in ihre
    Dokumente und/oder Transaktionen zu integrieren. Dabei werden die
    notwendigen Attribute durch die Brieftasche dem Qualifizierten
    Vertrauensdiensteanbieter zur Verfügung gestellt, der die
    eigentliche elektronische Signatur erzeugt.

6.  ***Digitales Rezept (ePrescription)***: Dank der Verknüpfung der
    EUdi-Brieftasche mit dem Dokumentenaustausch-System eHDSI (eHealth
    Digital Service Infrastructure) können die Nutzendenden Zugriff auf
    ihre Gesundheitsdaten erhalten und gleichzeitig die Kontrolle
    darüber bewahren, wer auf diese Daten zugreifen kann. Mithilfe ihrer
    Personenidentifizierungsdaten (PID) mittels Online-Ausweisfunktion
    könnten die Nutzenden medizinischem Fachpersonal den Zugriff auf
    ihre Rezepte gewähren. Das E-Rezept ermöglicht darüber hinaus
    weitere digitale Anwendungen, von der Medikationserinnerung bis hin
    zum Medikationsplan und einen Wechselwirkungscheck. Aktuell
    beteiligt sich Deutschland innerhalb des Konsortiums POTENTIAL nicht
    an der Ausgestaltung dieses Anwendungsfalls.

### Weitere Use Cases

Über die vorgenannten Use Cases hinaus bestehen weitere mögliche Use
Cases, die für eine Integration in die EUdi-Brieftasche in Frage kommen
könnten, darunter (ohne Anspruch auf Vollständigkeit):

- Vorlage eines Berufsnachweises (z.B. elektronischer Heilberufsausweis zum Erwerb von Medikamenten oder um eine elektronische Arbeitsunfähigkeitsbescheinigung auszustellen)
- Nachweis der Elternschaft für die Berechtigung von staatlichen Leistungen
- Vorlage des Nachweises eines Führungszeugnisses für ein Ehrenamt oder eine Bewerbung
- Vorlage einer digitalen Immatrikulationsbescheinigung von Studierenden für entsprechende Berechtigungen
- Übermittlung eines Nachweises eines Hochschulzeugnisses für eine Bewerbung
- Erwerb von personalisierten Tickets (z.B. Mobilität, Kultur)
- Identifizierung, um ein Paket oder Einschreiben anzunehmen
- Identifizierung für Mail- oder Social Media-Konten, wenn das Passwort verloren gegangen ist, oder das Konto gehackt wurde
- Pseudonymer Altersnachweis für Online-Shopping mit entsprechender Altersgrenze (z.B. Alkohol)
- Durchführung einer Zahlung mit Hilfe von Bankkonten, die in der Brieftasche hinterlegt sind
- Digitaler Hotelmeldeschein für Gäste aus dem EU-Ausland zur Anmeldung im Hotel

Dabei ist zu beachten, dass die Use Cases zum einen immer so
datensparsam ausgestaltet werden sollen, wie möglich – d.h. wenn nur
bestimmte Datenpunkte erforderlich sind, dass ansonsten nur ein
Pseudonym Anwendung findet und die Person nicht identifizierbar ist. Zum
anderen soll die Berechtigung zum Abruf von hoheitlichen Nachweisen aus
der EUdi-Brieftasche staatlich reguliert sein – analog zu den
Berechtigungszertifikaten bei der eID. Das heißt, dass ein
Diensteanbieter u.a. ein berechtigtes Interesse nachweisen und ein
Datenschutzkonzept vorlegen muss, um Daten nur mit dem Einverständnis
der nutzenden Person abrufen zu können. Die qualifizierten Attribute,
die von staatlicher Seite für eine EUdi-Wallet zur Verfügung gestellt
werden sollen, finden sich in Anhang 6 der überarbeiteten
eIDAS-Verordnung.

Um die Erweiterbarkeit der prototypischen Infrastruktur für neue
Anwendungsfälle und neue Technologien zu unterstützen, sollte ein
kontinuierliches Anforderungs- und Releasemanagement auf Basis einer
umfassenden Dokumentation von existierenden und erwarteten
Anwendungsfällen aufgesetzt werden.

## Ausblick: Der weitere Prozess

### Studie zur Umsetzung von eIDAS 2.0

Zur Umsetzung ist eine Vielzahl von Aspekten zu bedenken, darunter
soziologische Bedarfe und regulatorische Anforderungen, aber auch die
Kostenstrukturen und Fragen des funktionalen Umfangs einer künftigen
Infrastruktur. Um diesen vielfältigen Anforderungen und
Rahmenbedingungen gerecht zu werden, soll eine umfassende Studie
durchgeführt werden. Die Studie soll sich unter anderem mit den
folgenden Themenbereichen auseinandersetzen:

- **Identifizierung relevanter Akteure**: Die Ausgestaltung des
  Ökosystems soll unter Konsultation relevanter Akteure, die im Rahmen
  der Studie identifiziert werden sollen, erfolgen. Bei der Betrachtung
  werden sowohl privatwirtschaftliche Akteure als auch Akteure aus der
  Zivilgesellschaft berücksichtigt.

- **Anforderungen an Bund, Länder und Kommunen**: Welche Register in
  Deutschland könnten für die Umsetzung des Anhang VI im Hinblick auf
  Attributsbestätigungen betroffen sein? Wie hoch wird der Aufwand bei
  der Aufsicht der BNetzA und des BSI (+ ggf. weitere Behörden) über die
  Relying Parties, die Brieftasche, die Registerstellen und die
  internationale Zusammenarbeit eingeschätzt (Personalaufstockung,
  Schaffung neuer Behörden notwendig?) Können alle Regelungen zu
  Formaten und Schnittstellen der authentischen Quellen durch ein
  Bundesgesetz geregelt werden?

- **Zeitliche Rahmenbedingungen und Umsetzungsdauer**: Die Umsetzung der
  eIDAS-Verordnung geht aufgrund der komplexen und vielschichtigen
  Ausprägung mit erheblichem Zeitaufwand einher. Im Rahmen der Studie
  soll daher unter anderem die notwendige Dauer der Konzeptionierung,
  der Entwicklung und der Zertifizierung von Komponenten bestimmt
  werden, um einen belastbaren Zeitplan zur Umsetzung zu erhalten.

- **Einbindung und Verantwortung staatlicher Stellen**: Neben der
  Klärung der Verantwortlichkeit der staatlichen Akteure bei der
  Konzeptionierung, Betrieb und Weiterentwicklung der Infrastruktur
  (vgl. Kapitel [Ökosystem für Digitale Identitäten](#%C3%B6kosystem-f%C3%BCr-digitale-identit%C3%A4ten)), soll darüber hinaus die Einbindung und Verantwortung
  staatlicher Stellen unter anderem bei einer möglichen Beaufsichtigung
  der Aussteller von EUdi-Brieftaschen und Überwachung der
  qualifizierten Vertrauensdiensteanbieter, untersucht werden, um den
  aktuell diskutierten Vorgaben der eIDAS 2.0-Verordnung Rechnung zu
  tragen.

- **Kosteneffektivität**: Für die Finanzierung der im Kapitel [Ökosystem für Digitale Identitäten](#%C3%B6kosystem-f%C3%BCr-digitale-identit%C3%A4ten)
  exemplarisch genannten Gestaltungsoptionen (staatliche Variante,
  privatwirtschaftliche Variante oder eine Kombination der genannten
  Varianten für das Ökosystem) ist es notwendig, eine belastbare
  Kostenschätzung zu erstellen. Die Studie soll neben einem
  Kostenvergleich der genannten Optionen, insbesondere eine Einschätzung
  für die langfristige Finanzierung der Brieftasche treffen. Ziel der
  Kostenbetrachtung soll eine transparente Darstellung unterschiedlicher
  Szenarien und die damit zusammenhängenden Kosten für die
  Steuerzahlenden vor dem Hintergrund der erreichbaren Wirkung, insb.
  der Nutzeradoption und des gesamtwirtschaftlichen Potentials, sein

- **Vereinbarkeit und Umsetzung von staatlichen und privatwirtschaftlichen Komponenten**: Im Falle der gemeinsamen Bereitstellung eines oder mehreren Produkten durch staatliche und privatwirtschaftliche Akteure muss die Vereinbarkeit der jeweiligen Komponenten bzw. Module betrachtet und mögliche gemeinsame Umsetzungsschritte definiert werden. Eine Prüfung ist sowohl für die Vereinbarkeit auf technischer Ebene als auch auf einer Vielzahl von Bereichen der operativen Ebene notwendig.

- **Mögliche privatwirtschaftliche Geschäftsmodelle von Wallets**:
  Sollte eine Variante mit privatwirtschaftlichen Akteuren für die
  Konzeptionierung, Entwicklung und Ausstellung von EUdi-Brieftaschen
  gewählt werden (Option 2 oder 3, vgl. Kapitel [Ökosystem für Digitale Identitäten](#%C3%B6kosystem-f%C3%BCr-digitale-identit%C3%A4ten)), muss sich mit der Frage
  nach möglichen Geschäftsmodellen auseinandergesetzt werden. Im Rahmen
  der Studie gilt es, für dieses Geschäftsmodell unter anderem die
  Finanzierung und den Betrieb sowie die Implikationen für einen
  diskriminierungsfreien Zugang für alle Wirtschaftsakteure zu prüfen.

### Offener Architekturprozess

Die Erstellung eines groben Architekturkonzepts für die prototypische
Infrastruktur und für einen Prototypen einer deutschen EUdi-Brieftasche
soll im engen Austausch mit der Öffentlichkeit stattfinden und im
Zeitraum von Juni 2023 bis November 2023 ablaufen. Dies beinhaltet einen
Austausch mit der Zivilgesellschaft, mit Verbänden und Unternehmen, mit
der Wissenschaft sowie mit Vertreterinnen und Vertretern der
Bundesländer und Kommunen im föderalen Miteinander auf staatlicher
Seite. Ein solcher Prozess beinhaltet die Bereitstellung von
Zwischenschritten im Architekturprozess auf OpenCoDE, begleitet durch
Fokustermine und regelmäßige Austauschformate mit Interessierten.
OpenCoDE soll als Austauschplattform dienen, die als zentrale Stelle für
das Einholen von Feedback fungieren soll. Das Bundesministerium des
Innern und für Heimat möchte damit einen Prozess gewährleisten, der
offen und transparent ist, Innovation ermöglicht und politische
Entscheidungen erlaubt, die unter Berücksichtigung unterschiedlicher
Interessen und der zur Verfügung stehenden Informationen getroffen
werden können.

Wer Anmerkungen, Kommentare oder eigene Vorschläge zu hier diskutierten
Punkten hat, kann sich gerne auf der OpenCoDE-Plattform zu Wort melden.

### Zeitplan des Konsultationsprozesses für ein Wallet-Konzept

Zentrales Element einer eIDAS 2.0-konformen Infrastruktur ist die
Bereitstellung einer EUdi-Brieftasche durch die Mitgliedsstaaten.

Bürgerrechtsorganisationen, Datenschützerinnen und Datenschützer und
IT-Expertinnen und Experten äußerten deutliche Kritik an dem Vorhaben.
Die Bundesregierung, die den Prozess zur Konzeption aus deutscher
Perspektive verantwortet, strebt daher einen offenen und partizipativen
Architekturprozess an, um eine Antwort auf die Anforderungen von
Europäischer Ebene zu finden. Diese Antwort soll das bestehende deutsche
eID-System berücksichtigen, die dezentrale deutsche Registerlandschaft
in den Blick nehmen und vor allem einen (Gegen-)Entwurf zu bisherigen
Konzepten einer Wallet liefern, der Kritikerinnen und Kritiker ernst
nimmt. Im weiteren Verlauf soll ein offener Entwicklungsprozess für eine
prototypische Infrastruktur inkl. App-Prototyp angeschlossen werden.

Das Projekt wird innerhalb der Bundesregierung vom Bundesministerium des
Innern und für Heimat (BMI) verantwortet, das in enger Abstimmung mit
dem interministeriellen GovLabDE Digitale Identitäten steht und mit
Auftragnehmern kooperiert, die bei Architektur, Konsultation und
Projektkoordination unterstützen.

Ziel dieses Dokuments ist die Definition sowohl der zeitlichen als auch
der inhaltlichen Involvierung von Anspruchs- und Interessensgruppen in
den Entwicklungsprozess eines Konzepts für eine prototypische
EUdi-Brieftasche samt Hintergrundsystemen in Deutschland.

<table>
<colgroup>
<col style="width: 17%" />
<col style="width: 82%" />
</colgroup>
<thead>
<tr class="header">
<th>07. Juni 2023</th>
<th>Veröffentlichung des Vorhabens mit Zeitplan bei OpenCoDE.</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>30. Juni 2023</td>
<td><p>Bewerbungsfrist für Anspruchs- und Interessensgruppen zur Abgabe
eines einseitigen Positionspapiers und Nennung eines Repräsentanten für
kommende Workshops</p>
<ul>
<li><p>Paralleles Führen von Gesprächen zu ehemaligen Projektbeteiligten
der Corona-Warn-App, Best Practices wie <u>Wir vs. Virus</u>, <u>Project
Together</u></p></li>
<li><p>Parallele Möglichkeit Kommentare einzureichen, Analyse über Topic
Modeling o. ä. bis zum 31. August als Input für Workshops mit
Bürgerinnen und Bürgern</p></li>
</ul></td>
</tr>
<tr class="even">
<td>14. Juli 2023</td>
<td>Öffentliche Bekanntmachung der ausgewerteten Positionspapiere und
Bekanntgabe der Einreicher</td>
</tr>
<tr class="odd">
<td>31. Juli 2023</td>
<td><p>Workshop 1 mit Vertretern der Anspruchs- und Interessensgruppen
aus den folgenden Bereichen, mit Vertreterinnen und Vertretern aus
Wissenschaft, Wirtschaft und Zivilgesellschaft, um übergreifende Aspekte
zu identifizieren:</p>
<ul>
<li><p>Datenschutz</p></li>
<li><p>Datensicherheit</p></li>
<li><p>IT-Sicherheit</p></li>
<li><p>Bürgerrecht</p></li>
<li><p>Open Source</p></li>
<li><p>Interoperabilität mit anderen EU-Mitgliedsstaaten</p></li>
</ul>
<p>Ziel des Workshops ist die Fertigstellung eines gemeinsam getragenen,
fünfseitigen Ergebnispapiers.</p></td>
</tr>
<tr class="even">
<td>15. August 2023</td>
<td><p>Workshop 2 mit Vertretern der Anspruchs- und Interessensgruppen,
um wirtschaftsnahe Aspekte (Geschäftsmodelle im und mit der Brieftasche)
und vor allem Anwendungsfälle zu identifizieren:</p>
<ul>
<li><p>E-Commerce</p></li>
<li><p>Stationärer Einzelhandel</p></li>
<li><p>Automobilwirtschaft</p></li>
<li><p>Reiseindustrie</p></li>
<li><p>Handwerk</p></li>
<li><p>Bankindustrie</p></li>
<li><p>Logistikindustrie</p></li>
<li><p>Versicherungsindustrie</p></li>
<li><p>Telekommunikationsindustrie</p></li>
<li><p>Energieindustrie</p></li>
</ul>
<p>Ziel des Workshops ist die Fertigstellung eines gemeinsam getragenen,
zehnseitigen Ergebnispapiers.</p></td>
</tr>
<tr class="odd">
<td>31. August 2023</td>
<td><p>Workshop 3 mit Bürgerinnen und Bürgern, um nutzernahe Aspekte zu
identifizieren (+ Expertinnen und Experten zu UX)</p>
<p>Ziel des Workshops ist die Fertigstellung eines gemeinsam getragenen,
fünfseitigen Ergebnispapiers.</p></td>
</tr>
<tr class="even">
<td>30. November 2023</td>
<td>Präsentation des Konzepts</td>
</tr>
</tbody>
</table>

Die Termine sind zunächst als Save the Date zu verstehen, weitere
Formate kommen ggf. nach Bedarf dazu und werden kommuniziert.
